package org.nrg.xnat.registermr;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_registermr", name = "XNAT 1.7 Register MR Plugin", description = "This is the XNAT 1.7 Register MR Plugin.")
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class RegisterMrPlugin {
}